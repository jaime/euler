package _01_multiples

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMultiples(t *testing.T) {
	tcs := map[string]struct {
		in   int
		want int
	}{
		"1":    {1, 0},
		"2":    {2, 0},
		"3":    {3, 0},
		"4":    {4, 3},
		"5":    {5, 3},
		"6":    {6, 8},
		"7":    {7, 14},
		"8":    {8, 14},
		"9":    {9, 14},
		"10":   {in: 10, want: 23},
		"100":  {in: 100, want: 2318},
		"1000": {in: 1000, want: 233168},
	}

	for tn, tc := range tcs {
		t.Run(tn, func(t *testing.T) {
			got := Multiples(tc.in)
			assert.Equal(t, tc.want, got)
		})
	}
}
