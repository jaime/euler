package _02_fibonacci

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFibonacci(t *testing.T) {
	tcs := map[string]struct {
		in   int
		want int
	}{
		"1":    {1, 1},
		"2":    {2, 1},
		"3":    {3, 2},
		"4":    {4, 3},
		"5":    {5, 5},
		"6":    {6, 8},
		"7":    {7, 13},
		"8":    {8, 21},
		"9":    {9, 34},
		"10":   {in: 10, want: 55},
		"100":  {in: 100, want: 0},
		"1000": {in: 1000, want: 0},
	}

	for tn, tc := range tcs {
		t.Run(tn, func(t *testing.T) {
			got := Fibonacci(tc.in)
			assert.Equal(t, tc.want, got)
		})
	}
}
